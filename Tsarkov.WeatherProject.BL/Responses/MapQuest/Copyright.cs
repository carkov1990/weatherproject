namespace Tsarkov.WeatherProject.Models
{
	public class Copyright
	{
		public string text { get; set; }
		public string imageUrl { get; set; }
		public string imageAltText { get; set; }
	}
}