namespace Tsarkov.WeatherProject.Models
{
	public class Options
	{
		public int maxResults { get; set; }
		public bool thumbMaps { get; set; }
		public bool ignoreLatLngInput { get; set; }
	}
}