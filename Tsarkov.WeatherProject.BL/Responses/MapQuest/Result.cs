using System.Collections.Generic;

namespace Tsarkov.WeatherProject.Models
{
	public class Result
	{
		public ProvidedLocation providedLocation { get; set; }
		public IList<Location> locations { get; set; }
	}
}