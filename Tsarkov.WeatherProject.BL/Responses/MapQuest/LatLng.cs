namespace Tsarkov.WeatherProject.Models
{
	public class LatLng
	{
		public double lat { get; set; }
		public double lng { get; set; }
	}
}