namespace Tsarkov.WeatherProject.Models
{
	public class DisplayLatLng
	{
		public double lat { get; set; }
		public double lng { get; set; }
	}
}