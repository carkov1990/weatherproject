using System.Collections.Generic;

namespace Tsarkov.WeatherProject.Models
{
	public class MapQuestResponse
	{
		public Info info { get; set; }
		public Options options { get; set; }
		public IList<Result> results { get; set; }
	}
}