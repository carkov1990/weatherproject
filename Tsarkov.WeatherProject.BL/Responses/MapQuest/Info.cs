using System.Collections.Generic;

namespace Tsarkov.WeatherProject.Models
{
	public class Info
	{
		public int statuscode { get; set; }
		public Copyright copyright { get; set; }
		public IList<object> messages { get; set; }
	}
}