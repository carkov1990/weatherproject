namespace Tsarkov.WeatherProject.BL.Responses.WeatherBit
{
	/// <summary>
	/// Класс ответа WeatherBit
	/// </summary>
	public class WeatherBitItem
	{
		public float Temp { get; set; }
	}
}