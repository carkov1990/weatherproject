using System.Collections.Generic;
using Tsarkov.WeatherProject.Models;

namespace Tsarkov.WeatherProject.BL.Responses.WeatherBit
{
	/// <summary>
	/// Ответ WeatherBit
	/// </summary>
	public class WeatherBitResponse : BaseResponse
	{
		public int Count { get; set; }
		public IList<WeatherBitItem> Data { get; set; }
	}
}