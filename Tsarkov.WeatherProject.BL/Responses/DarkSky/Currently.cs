namespace Tsarkov.WeatherProject.BL.Responses.DarkSky
{
	public class Currently
	{
		public int Time { get; set; }
		public float Temperature { get; set; }
	}
}