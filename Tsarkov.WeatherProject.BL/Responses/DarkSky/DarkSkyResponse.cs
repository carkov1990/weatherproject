namespace Tsarkov.WeatherProject.BL.Responses.DarkSky
{
	public class DarkSkyResponse
	{
		public int Latitude { get; set; }
		public int Longitude { get; set; }
		public string TimeZone { get; set; }
		public Currently Currently { get; set; }
		public int Offset { get; set; }
	}
}