namespace Tsarkov.WeatherProject.BL
{
	/// <summary>
	/// Интерфейс хелпера для получения координат
	/// </summary>
	public interface ICoordinateHelper
	{
		/// <summary>
		/// Получение координат по названию города
		/// </summary>
		/// <param name="name">Название города</param>
		/// <returns>Широта, Долгота</returns>
		(double?, double?) GetCoordinatesByCityName(string name);
	}
}