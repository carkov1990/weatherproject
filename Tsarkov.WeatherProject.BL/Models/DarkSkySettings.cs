using System;

namespace Tsarkov.WeatherProject.BL.Models
{
	/// <summary>
	/// Класс настроек для DarkSky
	/// </summary>
	public class DarkSkySettings : BaseSettings
	{
		/// <inheritdoc cref="BaseSettings.GenerateUri"/>
		public override string GenerateUri(string arg)
		{
			return $"{Url}/{Key}/{arg},{(int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds}?units=si&exclude=hourly,daily,flags";
		}
	}
}