namespace Tsarkov.WeatherProject.BL.Models
{
	/// <summary>
	/// Базовый класс настроек
	/// </summary>
	public abstract class BaseSettings
	{
		/// <summary>
		/// Адрес
		/// </summary>
		public virtual string Url { get; set; }

		/// <summary>
		/// Api - key
		/// </summary>
		public virtual string Key { get; set; }

		/// <summary>
		/// Метод формирования Uri
		/// </summary>
		public abstract string GenerateUri(string arg);
	}
}