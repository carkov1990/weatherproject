namespace Tsarkov.WeatherProject.BL.Models
{
	/// <summary>
	/// Класс настроек для отправки Email
	/// </summary>
	public class EmailSettings
	{
		/// <summary>
		/// Email
		/// </summary>
		public string Email { get; set; }

		/// <summary>
		/// Адрес SMTP сервера
		/// </summary>
		public string SmtpServer { get; set; }

		/// <summary>
		/// Порт SMTP сервера
		/// </summary>
		public int SmtpServerPort { get; set; }

		/// <summary>
		/// Пароль
		/// </summary>
		public string Password { get; set; }
	}
}