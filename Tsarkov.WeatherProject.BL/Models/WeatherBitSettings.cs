using System.Text;

namespace Tsarkov.WeatherProject.BL.Models
{
	/// <summary>
	/// Класс настроект для WeatherBit 
	/// </summary>
	public class WeatherBitSettings : BaseSettings
	{
		/// <inheritdoc cref="BaseSettings.GenerateUri"/>
		public override string GenerateUri(string arg)
		{
			return $"{Url}?key={Key}&city={arg}";
		}
	}
}