namespace Tsarkov.WeatherProject.BL.Models
{
	/// <summary>
	/// Класс настроект для MapQuest 
	/// </summary>
	public class MapQuestSettings : BaseSettings
	{
		/// <inheritdoc cref="BaseSettings.GenerateUri"/>
		public override string GenerateUri(string arg)
		{
			return $"{Url}?key={Key}&location={arg}";
		}
	}
}