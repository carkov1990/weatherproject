using System;
using System.Linq;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NLog;
using Polly;
using Tsarkov.WeatherProject.BL.Models;
using Tsarkov.WeatherProject.Models;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.BL
{
	/// <summary>
	/// Класс хелпер для получения координат
	/// </summary>
	public class CoordinateHelper : ICoordinateHelper
	{
		private static ILogger Logger = LogManager.GetCurrentClassLogger();
		private readonly IConfiguration _configuration;

		/// <summary>
		/// .ctor
		/// </summary>
		public CoordinateHelper(IConfiguration configuration)
		{
			_configuration = configuration ?? throw new ArgumentNullException(nameof(_configuration));
		}

		/// <inheritdoc cref="ICoordinateHelper.GetCoordinatesByCityName"/>
		public (double?, double?) GetCoordinatesByCityName(string name)
		{
			double? latitude = null;
			double? longitude = null;
			var mapQuestSettings = _configuration.GetSection("AppSettings:MapQuest").Get<MapQuestSettings>();
			var polly = Policy
				.Handle<Exception>()
				.WaitAndRetry(3, retryAttempt => TimeSpan.FromSeconds(1));
			try
			{
				polly.Execute(() =>
				{
					var client = new HttpClient();
					var response = client.GetStringAsync(mapQuestSettings.GenerateUri(name)).GetAwaiter()
						.GetResult();
					var mapQuestResponse = JsonConvert.DeserializeObject<MapQuestResponse>(response);
					latitude = mapQuestResponse.results.First().locations.First().latLng.lat;
					longitude = mapQuestResponse.results.First().locations.First().latLng.lng;
				});
			}
			catch (Exception ex)
			{
				Logger.Error(ex);
			}

			return (latitude, longitude);
		}
	}
}