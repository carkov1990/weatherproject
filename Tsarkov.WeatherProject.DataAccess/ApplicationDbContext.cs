﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.DataAccess
{
	/// <summary>
	/// Контекст БД
	/// </summary>
	public class ApplicationDbContext : IdentityDbContext
	{
		/// <summary>
		/// .ctor
		/// </summary>
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
		}

		/// <summary>
		/// Таблица городов
		/// </summary>
		public DbSet<City> Cities { get; set; }

		/// <summary>
		/// Таблица город - пользователь
		/// </summary>
		public DbSet<CityByUser> CitiesByUsers { get; set; }

		/// <summary>
		/// Таблица погода - город по времени
		/// </summary>
		public DbSet<WeatherInCity> WeathersInCities { get; set; }
	}
}