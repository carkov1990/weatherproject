using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Tsarkov.WeatherProject.Models.DbModels
{
	/// <summary>
	/// Модель связки город - пользователь
	/// </summary>
	public class CityByUser
	{
		/// <summary>
		/// Идентификтор
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		/// <summary>
		/// Пользователь
		/// </summary>
		[ForeignKey("UserId")]
		public IdentityUser User { get; set; }

		/// <summary>
		/// Идентификатор пользователя
		/// </summary>
		public string UserId { get; set; }

		/// <summary>
		/// Город
		/// </summary>
		[ForeignKey("CityId")]
		public City City { get; set; }

		/// <summary>
		/// Идентификатор города
		/// </summary>
		public int CityId { get; set; }
	}
}