using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tsarkov.WeatherProject.Models.DbModels
{
	/// <summary>
	/// Класс модели города
	/// </summary>
	public class City
	{
		/// <summary>
		/// Идентификатор
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		/// <summary>
		/// Название
		/// </summary>
		[MaxLength(250)]
		public string Name { get; set; }

		/// <summary>
		/// Широта
		/// </summary>
		public double Latitude { get; set; }

		/// <summary>
		/// Долгота
		/// </summary>
		public double Longitude { get; set; }
	}
}