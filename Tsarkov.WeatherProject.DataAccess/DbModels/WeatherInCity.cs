using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tsarkov.WeatherProject.Models.DbModels
{
	/// <summary>
	/// Модель температуры по городам
	/// </summary>
	public class WeatherInCity
	{
		/// <summary>
		/// Идентификатор
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		/// <summary>
		/// Идентификатор города
		/// </summary>
		public int CityId { get; set; }

		/// <summary>
		/// Город
		/// </summary>
		[ForeignKey("CityId")]
		public City City { get; set; }

		/// <summary>
		/// Температура
		/// </summary>
		public float Temperature { get; set; }

		/// <summary>
		/// Дата сбора данных
		/// </summary>
		public DateTime Date { get; set; }
	}
}