using System;
using System.Collections.Generic;
using System.Linq;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.DataAccess.Helpers
{
	/// <summary>
	/// Класс хелпера для погоды
	/// </summary>
	public class WeatherHelper : IWeatherHelper
	{
		private readonly ApplicationDbContext _context;

		/// <summary>
		/// .ctor
		/// </summary>
		public WeatherHelper(ApplicationDbContext context)
		{
			_context = context ?? throw new ArgumentNullException(nameof(context));
		}
		
		/// <inheritdoc cref="IWeatherHelper.GetWeatherInCities"/>
		public IList<WeatherInCity> GetWeatherInCities(int cityId)
		{
			return _context.WeathersInCities.Where(w => w.CityId == cityId).OrderByDescending(c => c.Date).ToList();
		}

		/// <inheritdoc cref="IWeatherHelper.GetLastWeatherInCities"/>
		public WeatherInCity GetLastWeatherInCities(int cityId)
		{
			return _context.WeathersInCities.Where(w => w.CityId == cityId).OrderByDescending(c => c.Date).FirstOrDefault();
		}
	}
}