using System.Collections.Generic;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.DataAccess.Helpers
{
	/// <summary>
	/// Интерфейс для работы с городами
	/// </summary>
	public interface ICityHelper
	{
		/// <summary>
		/// Метод получения городов
		/// </summary>
		/// <returns></returns>
		IList<City> GetCities();

		/// <summary>
		/// Метод получения городов
		/// </summary>
		/// <returns></returns>
		IList<int> GetCityIdsMonitored(string userId);

		/// <summary>
		/// Метод добавления города в БД
		/// </summary>
		/// <param name="city"></param>
		/// <returns></returns>
		bool AddCity(City city);

		/// <summary>
		/// Метод добавления города в подборку к пользователю
		/// </summary>
		bool AddCityToMonitoring(int cityId, string userId);

		/// <summary>
		/// Метод получения города по идентификатору
		/// </summary>
		/// <param name="cityId"></param>
		City GetCityById(int cityId);
	}
}