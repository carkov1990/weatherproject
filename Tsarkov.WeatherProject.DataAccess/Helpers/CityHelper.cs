using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.DataAccess.Helpers
{
	/// <summary>
	/// Класс работы с городами
	/// </summary>
	public class CityHelper : ICityHelper
	{
		private readonly ApplicationDbContext _context;

		/// <summary>
		/// .ctor
		/// </summary>
		public CityHelper(ApplicationDbContext context)
		{
			_context = context ?? throw new ArgumentNullException(nameof(context));
		}

		/// <inheritdoc cref="ICityHelper.GetCities"/>
		public IList<City> GetCities()
		{
			return _context.Cities.AsNoTracking().ToList();
		}

		/// <inheritdoc cref="ICityHelper.GetCityIdsMonitored"/>
		public IList<int> GetCityIdsMonitored(string userId)
		{
			return _context.CitiesByUsers.Where(c => c.UserId == userId).Select(c => c.CityId).ToList();
		}

		/// <inheritdoc cref="ICityHelper.AddCity"/>
		public bool AddCity(City city)
		{
			if (city == null)
			{
				throw new ArgumentNullException(nameof(city));
			}

			if (_context.Cities.Any(c =>
				c.Latitude == city.Latitude && c.Longitude == city.Longitude && c.Name == city.Name))
			{
				return true;
			}

			_context.Add(city);
			return _context.SaveChanges() > 0;
		}

		/// <inheritdoc cref="ICityHelper.AddCityToMonitoring"/>
		public bool AddCityToMonitoring(int cityId, string userId)
		{
			if (cityId <= 0)
			{
				throw new ArgumentException(nameof(cityId));
			}

			if (String.IsNullOrWhiteSpace(userId))
			{
				throw new ArgumentException(nameof(userId));
			}

			if (_context.CitiesByUsers.Any(c => c.CityId == cityId && c.UserId == userId))
			{
				return true;
			}

			_context.CitiesByUsers.Add(new CityByUser()
			{
				CityId = cityId,
				UserId = userId
			});
			return _context.SaveChanges() > 0;
		}

		/// <inheritdoc cref="ICityHelper.GetCityById"/>
		public City GetCityById(int cityId)
		{
			return _context.Cities.FirstOrDefault(c => c.Id == cityId);
		}
	}
}