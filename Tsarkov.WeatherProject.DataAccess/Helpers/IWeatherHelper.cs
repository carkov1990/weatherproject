using System.Collections.Generic;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.DataAccess.Helpers
{
	/// <summary>
	/// Интерфейс хелпера для получения погоды 
	/// </summary>
	public interface IWeatherHelper
	{
		/// <summary>
		/// Метод получения погоды по городу
		/// </summary>
		IList<WeatherInCity> GetWeatherInCities(int cityId);

		/// <summary>
		/// Метод получения последнего состояния погоды по городу
		/// </summary>
		WeatherInCity GetLastWeatherInCities(int cityId);
	}
}