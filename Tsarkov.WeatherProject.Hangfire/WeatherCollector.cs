﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using FluentEmail.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NLog;
using Polly;
using Tsarkov.WeatherProject.BL.Models;
using Tsarkov.WeatherProject.BL.Responses.DarkSky;
using Tsarkov.WeatherProject.BL.Responses.WeatherBit;
using Tsarkov.WeatherProject.DataAccess;
using Tsarkov.WeatherProject.Models;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.Hangfire
{
	/// <summary>
	/// Класс сборщика погоды
	/// </summary>
	public class WeatherCollector : IWeatherCollector
	{
		private readonly ApplicationDbContext _context;
		private readonly IConfiguration _configuration;
		private readonly IFluentEmailFactory _factory;

		private static ILogger Logger = LogManager.GetCurrentClassLogger(); 
		
		/// <summary>
		/// .ctor
		/// </summary>
		public WeatherCollector(ApplicationDbContext context, IConfiguration configuration, IFluentEmailFactory factory)
		{
			_context = context ?? throw new ArgumentNullException(nameof(context));
			_configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
			_factory = factory ?? throw new ArgumentNullException(nameof(factory));
		}

		/// <inheritdoc cref="IWeatherCollector.Collect"/>
		public void Collect()
		{
			var weatherBitSettings = _configuration.GetSection("AppSettings:WeatherBit").Get<WeatherBitSettings>();
			var darkSkySettings = _configuration.GetSection("AppSettings:DarkSky").Get<DarkSkySettings>();
			var cities = _context.Cities.AsNoTracking().ToList();
			ConcurrentBag<WeatherInCity> bag = new ConcurrentBag<WeatherInCity>();
			
			Parallel.ForEach(cities, city =>
			{
				float? temperature = null;
				var polly = Policy
					.Handle<Exception>()
					.WaitAndRetry(3, retryAttempt => TimeSpan.FromSeconds(1));
				try
				{
					
					polly.Execute(() =>
					{
						var client = new HttpClient();
						var response = client.GetStringAsync(weatherBitSettings.GenerateUri(city.Name)).GetAwaiter()
							.GetResult();
						var weatherBitResponse = JsonConvert.DeserializeObject<WeatherBitResponse>(response);
						temperature = weatherBitResponse.Data.FirstOrDefault()?.Temp;
					});
				}
				catch (Exception ex)
				{
					try
					{
						polly.Execute(() =>
						{
							var client = new HttpClient();
							var response = client.GetStringAsync(darkSkySettings.GenerateUri($"{city.Latitude},{city.Longitude}")).GetAwaiter()
								.GetResult();
							var darkSkyResponse = JsonConvert.DeserializeObject<DarkSkyResponse>(response);
							temperature = darkSkyResponse.Currently?.Temperature;
						});
					}
					catch (Exception exc)
					{
						Logger.Error(exc);
					}
				}

				if (temperature != null)
				{
					bag.Add(new WeatherInCity()
					{
						CityId = city.Id,
						Date = DateTime.Now,
						Temperature = temperature.Value
					});
				}
			});
			
			if (bag.Count > 0)
			{
				var listChangedCities = new List<int>();
				foreach (var weatherInCity in bag)
				{
					var lastTemperature = _context.WeathersInCities.OrderByDescending(c=>c.Date).Select(c=>c.Temperature).FirstOrDefault();
					if (lastTemperature != weatherInCity.Temperature)
					{
						listChangedCities.Add(weatherInCity.CityId);
					}
				}

				if (listChangedCities.Count > 0)
				{
					var groupedCitiesByUsers = _context.CitiesByUsers
						.Include(c=>c.City)
						.Include(c=>c.User)
						.AsNoTracking()
						.GroupBy(user => user.User.Email).ToList();

					foreach (var citiesByUser in groupedCitiesByUsers)
					{
						var selectedCities = citiesByUser.Where(c => listChangedCities.Contains(c.CityId)).Select(c=>c.City);
						StringBuilder sb = new StringBuilder();
						sb.AppendLine($"Температура в городах отслеживаемых Вами изменилась.");
						foreach (var city in selectedCities)
						{
							sb.AppendLine($"{city.Name} : {bag.FirstOrDefault(b=>b.CityId == city.Id)?.Temperature.ToString()} C");
						}
						var email = _factory.Create();
						email.Subject("Изменение температуры").To(citiesByUser.Key).Body(sb.ToString()).Send();
					}	
				}
				_context.WeathersInCities.AddRange(bag.ToArray());
				_context.SaveChanges();
			}
		}
	}
}