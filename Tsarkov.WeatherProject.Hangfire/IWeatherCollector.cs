namespace Tsarkov.WeatherProject.Hangfire
{
	/// <summary>
	/// Интерфейс сборщика погоды
	/// </summary>
	public interface IWeatherCollector
	{
		/// <summary>
		/// Метод сбора данных по погоде
		/// </summary>
		void Collect();
	}
}