﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace Tsarkov.WeatherProject.Host
{
	/// <summary>
	/// Класс program
	/// </summary>
	public class Program
	{
		/// <summary>
		/// Входная точка программы
		/// </summary>
		public static void Main(string[] args)
		{
			CreateWebHostBuilder(args).Build().Run();
		}

		/// <summary>
		/// Метод конфигурирования IWebHostBuilder 
		/// </summary>
		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>().ConfigureLogging(logging =>
				{
					logging.ClearProviders();
					logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Information);
				})
				.UseNLog();
	}
}