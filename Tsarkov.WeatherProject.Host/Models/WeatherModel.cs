using System.Collections.Generic;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.Models
{
	public class WeatherModel
	{
		public City City { get; set; }

		public IList<WeatherInCity> WeaterInCity { get; set; } = new List<WeatherInCity>();
	}
}