using System;
using System.Runtime.InteropServices.WindowsRuntime;
using NLog.LayoutRenderers;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.Models
{
	/// <summary>
	/// Модель города
	/// </summary>
	public class CityModel
	{
		/// <summary>
		/// Идентификатор
		/// </summary>
		public int Id { get; set; }
		
		/// <summary>
		/// Название
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Широта
		/// </summary>
		public double? Latitude { get; set; }

		/// <summary>
		/// Долгота
		/// </summary>
		public double? Longitude { get; set; }

		/// <summary>
		/// Признак присутсвия города в мониторинге
		/// </summary>
		public bool IsMonitored { get; set; }

		/// <summary>
		/// Метод проверки валидности
		/// </summary>
		/// <returns></returns>
		public bool IsValid()
		{
			if (string.IsNullOrWhiteSpace(Name))
			{
				return false;
			}

			if (Latitude == null || Latitude < -90 || Latitude > 90)
			{
				return false;
			}

			if (Longitude == null || Longitude < -180 || Longitude > 180)
			{
				return false;
			}

			return true;
		}

		/// <summary>
		/// Метод преобразования из пользовательской модели города в модель БД
		/// </summary>
		/// <returns></returns>
		public City GetDbCity()
		{
			return new City()
			{
				Latitude = this.Latitude ?? 0,
				Name = this.Name,
				Longitude = this.Longitude ?? 0
			};
		}
	}
}