﻿using System.Net;
using System.Net.Mail;
using System.Threading;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using Tsarkov.WeatherProject.BL;
using Tsarkov.WeatherProject.BL.Models;
using Tsarkov.WeatherProject.DataAccess;
using Tsarkov.WeatherProject.DataAccess.Helpers;
using Tsarkov.WeatherProject.Hangfire;

namespace Tsarkov.WeatherProject.Host
{
	public class Startup
	{
		private static ILogger Logger = LogManager.GetCurrentClassLogger();
		private static object lockerHangfire = new object();
		
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }
		private static ServiceProvider _serviceProvider;


		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.Configure<CookiePolicyOptions>(options =>
			{
				// This lambda determines whether user consent for non-essential cookies is needed for a given request.
				options.CheckConsentNeeded = context => true;
				options.MinimumSameSitePolicy = SameSiteMode.None;
			});

			services.AddDbContext<ApplicationDbContext>(options =>
				options.UseSqlite(
					Configuration.GetConnectionString("DefaultConnection")));
			services.AddDefaultIdentity<IdentityUser>()
				.AddEntityFrameworkStores<ApplicationDbContext>();

			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

			services.AddHangfire(configuration => configuration.UseMemoryStorage());
			
			var emailSettings = Configuration.GetSection("AppSettings:EmailSettings").Get<EmailSettings>();
			
			services
				.AddFluentEmail(emailSettings.Email, "Weather Project")
				.AddSmtpSender(() =>
				{
					var smtpSender = new SmtpClient(emailSettings.SmtpServer, emailSettings.SmtpServerPort);
					smtpSender.Credentials = new NetworkCredential(emailSettings.Email, emailSettings.Password);
					smtpSender.EnableSsl = true;
					return smtpSender;
				});
			
			services.AddSingleton<IWeatherCollector, WeatherCollector>();
			services.AddScoped<ICityHelper, CityHelper>();
			services.AddScoped<IWeatherHelper, WeatherHelper>();
			services.AddScoped<ICoordinateHelper, CoordinateHelper>();
			
			_serviceProvider = services.BuildServiceProvider();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseDatabaseErrorPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				app.UseHsts();
			}

			app.UseStaticFiles();

			app.UseAuthentication();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");
			});

			GlobalConfiguration.Configuration.UseMemoryStorage();
			
			app.UseHangfireDashboard();
			app.UseHangfireServer();

			RecurringJob.AddOrUpdate(() => RunCollectWeather(), Cron.MinuteInterval(15));
		}

		public void RunCollectWeather()
		{
			if (Monitor.TryEnter(lockerHangfire))
			{
				try
				{
					var collector = _serviceProvider.GetService<IWeatherCollector>();
					collector.Collect();
				}
				finally 
				{
					Monitor.Exit(lockerHangfire);
				}
			}
			else
			{
				Logger.Warn("Запуск задачи Hangfire наложился на предыдущую");
			}
		}
	}
}