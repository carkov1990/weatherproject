using System;
using Microsoft.AspNetCore.Mvc;
using Tsarkov.WeatherProject.DataAccess.Helpers;
using Tsarkov.WeatherProject.Models;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.Host.Controllers
{
	public class WeatherController : Controller
	{
		private readonly ICityHelper _cityHelper;
		private readonly IWeatherHelper _weatherHelper;
		
		/// <summary>
		/// .ctor
		/// </summary>
		public WeatherController(ICityHelper cityHelper, IWeatherHelper weatherHelper)
		{
			_weatherHelper = weatherHelper ?? throw new ArgumentNullException(nameof(weatherHelper));
			_cityHelper = cityHelper ?? throw new ArgumentNullException(nameof(cityHelper));
		}
		
		/// <summary>
		/// Метод получения погоды по городу
		/// </summary>
		[HttpGet]
		public IActionResult Index([FromQuery]int cityId)
		{
			var weatherModel = new WeatherModel();
			var city = _cityHelper.GetCityById(cityId);
			weatherModel.City = city;
			var listWeathers = _weatherHelper.GetWeatherInCities(cityId);
			weatherModel.WeaterInCity = listWeathers;
			return View(weatherModel);
		}
	}
}