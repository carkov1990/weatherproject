﻿using System.Diagnostics;
using FluentEmail.Core;
using Microsoft.AspNetCore.Mvc;
using NLog;
using Tsarkov.WeatherProject.Models;
using ILogger = NLog.ILogger;

namespace Tsarkov.WeatherProject.Host.Controllers
{
	public class HomeController : Controller
	{
		public HomeController(IFluentEmailFactory factory)
		{
			var email = factory.Create();
			email.Body("asdasd").To("carkov1990@gmail.com").Send();
		}
		
		private static ILogger Logger = LogManager.GetCurrentClassLogger();

		public IActionResult Index()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
		}
	}
}