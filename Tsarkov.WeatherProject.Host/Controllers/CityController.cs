using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Tsarkov.WeatherProject.BL;
using Tsarkov.WeatherProject.DataAccess.Helpers;
using Tsarkov.WeatherProject.Models;
using Tsarkov.WeatherProject.Models.DbModels;

namespace Tsarkov.WeatherProject.Host.Controllers
{
	[Authorize]
	public class CityController : Controller
	{
		private readonly ICityHelper _cityHelper;
		private readonly ICoordinateHelper _coordinateHelper;

		/// <summary>
		/// .ctor
		/// </summary>
		public CityController(ICityHelper cityHelper, ICoordinateHelper coordinateHelper)
		{
			_cityHelper = cityHelper ?? throw new ArgumentNullException(nameof(cityHelper));
			_coordinateHelper = coordinateHelper ?? throw new ArgumentNullException(nameof(coordinateHelper));
		}

		/// <summary>
		/// Метод получения Index View
		/// </summary>
		[HttpGet]
		public IActionResult Index()
		{
			var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
			var monitoredIds = _cityHelper.GetCityIdsMonitored(userId);
			var listCities = _cityHelper.GetCities().Select(c => new CityModel()
			{
				Latitude = c.Latitude,
				Name = c.Name,
				IsMonitored = monitoredIds.Contains(c.Id),
				Longitude = c.Longitude,
				Id = c.Id
			}).ToList();
			return View(listCities);
		}

		/// <summary>
		/// Метод добавления города в БД
		/// </summary>
		[HttpPost]
		public IActionResult Add(CityModel city)
		{
			if (String.IsNullOrWhiteSpace(city.Name))
			{
				return Redirect("Index?status=AddError&reason=Name");
			}

			if (!city.IsValid())
			{
				var coordinates = _coordinateHelper.GetCoordinatesByCityName(city.Name);
				city.Latitude = coordinates.Item1;
				city.Longitude = coordinates.Item2;
				if (!city.IsValid())
				{
					return Redirect("Index?status=AddError&reason=Coordinates");
				}
			}

			_cityHelper.AddCity(city.GetDbCity());
			return Redirect("Index?status=AddSuccess");
		}

		/// <summary>
		/// Метод добавления города в подборку к пользователю
		/// </summary>
		[HttpPost]
		public IActionResult AddToMonitoring(CityByUser weatherInCity)
		{
			var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
			_cityHelper.AddCityToMonitoring(weatherInCity.CityId, userId);
			return Ok();
		}
	}
}