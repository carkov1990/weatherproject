# WeatherProject

* Команды для запуска

dotnet build

dotnet publish -o \<directory\>

cd \<directory\>

dotnet Tsarkov.WeatherProject.Host.dll


* Открыть браузер по адресу http://localhost:5000 или https://localhost:5001
* Зарегистрироваться
* Авторизоваться
* Получить список городов
* Добавить город
* Добавить город в подборку отслеживания
* Получить погоду по городу